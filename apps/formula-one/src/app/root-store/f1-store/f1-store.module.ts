import { NgModule } from "@angular/core";
import { EffectsModule } from "@ngrx/effects";
import { StoreModule } from "@ngrx/store";
import { Reducers } from "../root-store.models";
import { F1Effects } from "./effects";
import { f1Reducers } from "./reducers";

@NgModule({
    imports:[StoreModule.forFeature(Reducers.FORMULA_ONE,f1Reducers),EffectsModule.forFeature(F1Effects)]
})
export class F1StoreModule {

}