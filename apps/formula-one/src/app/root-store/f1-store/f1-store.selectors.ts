import { createFeatureSelector, createSelector } from "@ngrx/store";
import { Reducers } from "../root-store.models";
import { IF1State } from "./reducers";

export const F1State = createFeatureSelector<IF1State>(Reducers.FORMULA_ONE);

export const getAllTeamsSelector = createSelector(F1State, (state) => {
    return state.teamsState.teams;
});

export const getAllPilotsSelector = createSelector(F1State, (state) => {
    return state.pilotsState.pilots;
});

