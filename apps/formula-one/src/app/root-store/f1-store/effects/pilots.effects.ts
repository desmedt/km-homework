import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { catchError, exhaustMap, map, of } from "rxjs";
import { F1Service } from "../../../formula-one/f1.service";
import { pilotsActions, teamsActions } from "../actions";

@Injectable()
export class PilotsEffects {

    getAllPilots$ = createEffect(() =>
    this._actions.pipe(
        ofType(
            pilotsActions.getAllPilots,
            pilotsActions.addNewPilotSuccess,
            pilotsActions.editPilotSuccess,
            pilotsActions.deletePilotSuccess,
            teamsActions.addNewTeamSuccess,
            teamsActions.editTeamSuccess,
            teamsActions.deleteTeamSuccess),
        exhaustMap(() => this._f1Svc.getAllPilots().pipe(
            map((result) => pilotsActions.getAllPilotsSuccess({pilots: result})),
            catchError((err) => of(pilotsActions.getAllPilotsFail(err)))
        ))
    ));

    deletePilot$ = createEffect(() =>
    this._actions.pipe(
        ofType(pilotsActions.deletePilot),
        exhaustMap((action) => this._f1Svc.deletePilot(action.pilotId).pipe(
            map(() => pilotsActions.deletePilotSuccess({pilotId: action.pilotId})),
            catchError((err) => of(pilotsActions.deletePilotFail(err)))
        ))
    ));

    addNewPilot$ = createEffect(() =>
    this._actions.pipe(
        ofType(pilotsActions.addNewPilot),
        exhaustMap((action) => this._f1Svc.addPilot(action.newPilot).pipe(
            map((newPilot) => pilotsActions.addNewPilotSuccess({newPilot: newPilot})),
            catchError((err) => of(pilotsActions.addNewPilotFail(err)))
        ))
    ));

    editPilot$ = createEffect(() =>
    this._actions.pipe(
        ofType(pilotsActions.editPilot),
        exhaustMap((action) => this._f1Svc.editPilot(action.pilotId,action.pilot).pipe(
            map((result) => pilotsActions.editPilotSuccess({pilot: result})),
            catchError((err) => of(pilotsActions.editPilotFail(err)))
        ))
    ))

    constructor(private _actions: Actions, private _f1Svc: F1Service) {}
}