import { PilotsEffects } from "./pilots.effects";
import { TeamsEffects } from "./teams.effects";

export const F1Effects = [
    PilotsEffects,
    TeamsEffects
]