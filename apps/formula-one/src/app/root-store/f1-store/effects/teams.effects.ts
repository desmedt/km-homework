import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { of } from "rxjs";
import { catchError, exhaustMap, map } from "rxjs/operators";
import { F1Service } from "../../../formula-one/f1.service";
import { pilotsActions, teamsActions } from "../actions";

@Injectable()
export class TeamsEffects {


    getAllTeams$ = createEffect(() =>
    this._actions.pipe(
        ofType(
            teamsActions.getAllTeams,
            teamsActions.editTeamSuccess,
            teamsActions.deleteTeamSuccess,
            teamsActions.addNewTeamSuccess,
            pilotsActions.addNewPilotSuccess,
            pilotsActions.editPilotSuccess,
            pilotsActions.deletePilotSuccess),
        exhaustMap(() => this._f1Svc.getAllTeams().pipe(
            map((result) => teamsActions.getAllTeamsSuccess({teams: result})),
            catchError((err) => of(teamsActions.getAllTeamsFail(err)))
        ))
    ));

    deleteTeam$ = createEffect(() =>
    this._actions.pipe(
        ofType(teamsActions.deleteTeam),
        exhaustMap((action) => this._f1Svc.deleteTeam(action.teamId).pipe(
            map(() => teamsActions.deleteTeamSuccess({teamId: action.teamId})),
            catchError((err) => of(teamsActions.deleteTeamFail(err)))
        ))
    ));

    addNewTeam$ = createEffect(() =>
    this._actions.pipe(
        ofType(teamsActions.addNewTeam),
        exhaustMap((action) => this._f1Svc.addTeam(action.newTeam).pipe(
            map((newTeam) => teamsActions.addNewTeamSuccess({newTeam: newTeam})),
            catchError((err) => of(teamsActions.addNewTeamFail(err)))
        ))
    ));

    editTeam$ = createEffect(() =>
    this._actions.pipe(
        ofType(teamsActions.editTeam),
        exhaustMap((action) => this._f1Svc.editTeam(action.teamId,action.team).pipe(
            map( (result) => teamsActions.editTeamSuccess({team: result})),
            catchError((err) => of(teamsActions.editTeamFail(err))
        )))
    ));

    constructor(private _actions: Actions, private _f1Svc: F1Service) {}
}