import { IF1Team, IF1TeamRequest } from "@km-homework/api-interfaces";
import { createAction, props } from "@ngrx/store";


export const getAllTeams = createAction('[F1 Overview Component] Get All Teams');
export const getAllTeamsSuccess = createAction('[F1 Overview Component] Get All Teams Success', props<{teams: IF1Team[]}>());
export const getAllTeamsFail = createAction('[F1 Overview Component] Get All Teams Fail', props<{err: any}>());

export const getTeam = createAction('[F1 Overview Component] Get Team', props<{teamId: string}>());
export const getTeamSuccess = createAction('[F1 Overview Component] Get Team Success', props<{team: IF1Team}>());
export const getTeamFail = createAction('[F1 Overview Component] Get Team Fail', props<{err: any}>());

export const addNewTeam = createAction('[Add Dialog Component] Add New Team', props<{newTeam: IF1TeamRequest}>());
export const addNewTeamSuccess = createAction('[Add Dialog Component] Add New Team Success', props<{newTeam: IF1Team}>());
export const addNewTeamFail = createAction('[Add Dialog Component] Add New Team Fail', props<{err: any}>());

export const editTeam = createAction('[Edit Dialog Component] Edit Team',props<{teamId: string, team: IF1TeamRequest}>());
export const editTeamSuccess = createAction('[Edit Dialog Component] Edit Team Success', props<{team: IF1Team}>());
export const editTeamFail = createAction('[Edit Dialog Component] Edit Team Fail', props<{err: any}>());

export const deleteTeam = createAction('[F1 Overview Component] Delete Team', props<{teamId: string}>());
export const deleteTeamSuccess = createAction('[F1 Overview Component] Delete Team Success', props<{teamId: string}>());
export const deleteTeamFail = createAction('[F1 Overview Component] Delete Team Fail', props<{err: any}>());