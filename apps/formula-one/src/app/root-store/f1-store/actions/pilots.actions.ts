import { IF1Pilot, IF1PilotRequest } from "@km-homework/api-interfaces";
import { createAction, props } from "@ngrx/store";


export const getAllPilots = createAction('[F1 Overview Component] Get All Pilots');
export const getAllPilotsSuccess = createAction('[F1 Overview Component] Get All Pilots Success', props<{pilots: IF1Pilot[]}>());
export const getAllPilotsFail = createAction('[F1 Overview Component] Get All Pilots Fail', props<{err: any}>());

export const getPilot = createAction('[F1 Overview Component] Get Pilot', props<{pilotId: string}>());
export const getPilotSuccess = createAction('[F1 Overview Component] Get Pilot Success', props<{pilot: IF1Pilot}>());
export const getPilotFail = createAction('[F1 Overview Component] Get Pilot Fail', props<{err: any}>());

export const addNewPilot = createAction('[Add Dialog Component] Add New Pilot', props<{newPilot: IF1PilotRequest}>());
export const addNewPilotSuccess = createAction('[Add Dialog Component] Add New Pilot Success', props<{newPilot: IF1Pilot}>());
export const addNewPilotFail = createAction('[Add Dialog Component] Add New Pilot Fail', props<{err: any}>());

export const editPilot = createAction('[Edit Dialog Component] Edit Pilot', props<{pilotId: string,pilot: IF1PilotRequest}>());
export const editPilotSuccess = createAction('[Edit Dialog Component] Edit Pilot Success', props<{pilot: IF1Pilot}>());
export const editPilotFail = createAction('[Edit Dialog Component] Edit Pilot Fail', props<{err: any}>());

export const deletePilot = createAction('[F1 Overview Component] Delete Pilot', props<{pilotId: string}>());
export const deletePilotSuccess = createAction('[F1 Overview Component] Delete Pilot Success', props<{pilotId: string}>());
export const deletePilotFail = createAction('[F1 Overview Component] Delete Pilot Fail', props<{err: any}>());