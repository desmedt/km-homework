import { IF1Pilot } from "@km-homework/api-interfaces";
import { createReducer, on } from "@ngrx/store";
import { pilotsActions } from "../actions";


export interface IPilotsState {
    pilots: IF1Pilot[];
};

export const initialPilotsState: IPilotsState = {
    pilots: []
};

export const pilotsReducer = createReducer(
    initialPilotsState,
    on(pilotsActions.getAllPilotsSuccess, (state, action) => {
        return {...state, pilots: action.pilots}
    }),
    on(pilotsActions.deletePilotSuccess, (state, action) => {
        return {...state, pilots: state.pilots.filter(pilot => pilot.id !== action.pilotId)}
    }),
    on(pilotsActions.addNewPilotSuccess, (state, action) => {
        return {...state, pilots: [...state.pilots,action.newPilot]}
    }),
    on(pilotsActions.editPilotSuccess, (state, action) => {
        return {...state, pilots: state.pilots.map(pilot => pilot.id === action.pilot.id ? action.pilot : pilot)}
    })
);