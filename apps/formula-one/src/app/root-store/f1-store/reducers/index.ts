import { combineReducers } from "@ngrx/store";
import { IPilotsState, pilotsReducer } from "./pilots.reducer";
import { ITeamsState, teamsReducer } from "./teams.reducer";

export interface IF1State {
    pilotsState: IPilotsState,
    teamsState: ITeamsState    
}

export const f1Reducers = combineReducers({
    pilotsState: pilotsReducer,
    teamsState: teamsReducer
});