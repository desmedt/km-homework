import { IF1Team } from "@km-homework/api-interfaces";
import { createReducer, on } from "@ngrx/store";
import { teamsActions } from "../actions";


export interface ITeamsState {
    teams: IF1Team[];
}

export const initialTeamsState: ITeamsState = {
    teams: []   
}

export const teamsReducer = createReducer(
    initialTeamsState,
    on(teamsActions.getAllTeamsSuccess, (state, action) => {
        return { ...state, teams: action.teams}
    }),
    on(teamsActions.deleteTeamSuccess, (state, action) => {
        return { ...state, teams: state.teams.filter(team => team.id !== action.teamId)}
    }),
    on(teamsActions.addNewTeamSuccess, (state, action) => {
        return { ...state, teams: [...state.teams,action.newTeam]}
    }),
    on(teamsActions.editTeamSuccess, (state, action) => {
        return {...state, teams: state.teams.map(team => team.id === action.team.id ? action.team : team)}
    })
);

