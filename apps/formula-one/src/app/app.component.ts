import { Component } from '@angular/core';

@Component({
  selector: 'km-homework-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less'],
})
export class AppComponent {
}
