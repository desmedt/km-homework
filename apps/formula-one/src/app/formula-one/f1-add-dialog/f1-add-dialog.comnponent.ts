import { Component, Inject} from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { allEngines, IF1Pilot, IF1PilotRequest, IF1Team } from "@km-homework/api-interfaces";
import { Store } from "@ngrx/store";
import { Observable } from "rxjs";
import { pilotsActions, teamsActions } from "../../root-store/f1-store/actions";
import { getAllPilotsSelector, getAllTeamsSelector } from "../../root-store/f1-store/f1-store.selectors";
import { F1OverviewComponent } from "../f1-overview/f1-overview.component";
import { IAddDialogData, DIALOG_TYPES } from "../f1.models";

@Component({
    selector: 'f1-add-dialog',
    templateUrl: './f1-add-dialog.component.html',
    styleUrls: ['./f1-add-dialog.component.less']
})
export class F1AddDialogComponent {

    engines = allEngines;

    dialogTypes = DIALOG_TYPES;
    dialogFg: FormGroup;

    teams$: Observable<IF1Team[]>;
    pilots$: Observable<IF1Pilot[]>;


    constructor(
        public editDialogRef: MatDialogRef<F1OverviewComponent>,
        @Inject(MAT_DIALOG_DATA) public data: IAddDialogData,
        private _store: Store
      ) {
        this.teams$ = this._store.select(getAllTeamsSelector);
        this.pilots$ = this._store.select(getAllPilotsSelector);
        if(this.data.dialogType === DIALOG_TYPES.PILOT_DIALOG) {
            this.dialogFg = new FormGroup({
                name: new FormControl('',[Validators.required]),
                nationality: new FormControl('',[Validators.required]),
                team: new FormControl('',[Validators.required])
            });
        } else {
            this.dialogFg = new FormGroup({
                name: new FormControl('',[Validators.required]),
                engine: new FormControl('',[Validators.required]),
                pilotIds: new FormControl('',[Validators.required])
            });
        }
      }

    addNewEntity() {
        if(this.data.dialogType === DIALOG_TYPES.PILOT_DIALOG) {
            this._store.dispatch(pilotsActions.addNewPilot({newPilot: {
                name: this.dialogFg.controls['name'].value,
                nationality: this.dialogFg.controls['nationality'].value,
                teamId: this.dialogFg.controls['team'].value
            }}));
        } else {
            this._store.dispatch(teamsActions.addNewTeam({newTeam: {
                name: this.dialogFg.controls['name'].value,
                engine: this.dialogFg.controls['engine'].value,
                pilotIds: this.dialogFg.controls['pilotIds'].value
            }}))
        }
        this.editDialogRef.close();
    }

    closeDialog() {
        this.editDialogRef.close()
    }

    isFormValid() {
        return this.dialogFg.status === 'VALID' 
    }
}