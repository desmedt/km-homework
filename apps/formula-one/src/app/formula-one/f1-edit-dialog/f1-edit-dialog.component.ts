import { Component, Inject } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { allEngines, IF1Pilot, IF1Team } from "@km-homework/api-interfaces";
import { Store } from "@ngrx/store";
import { first, map, Observable } from "rxjs";
import { pilotsActions, teamsActions } from "../../root-store/f1-store/actions";
import { getAllPilotsSelector, getAllTeamsSelector} from "../../root-store/f1-store/f1-store.selectors";
import { F1OverviewComponent } from "../f1-overview/f1-overview.component";
import {DIALOG_TYPES, IEditDialogData } from "../f1.models";

@Component({
    selector: 'f1-edit-dialog',
    templateUrl: './f1-edit-dialog.component.html',
    styleUrls: ['./f1-edit-dialog.component.less']
})
export class F1EditDialogComponent {

    engines = allEngines;
    dialogTypes = DIALOG_TYPES;
    dialogFg: FormGroup = new FormGroup({});

    pilots$: Observable<IF1Pilot[]>;
    teams$: Observable<IF1Team[]>;

    constructor(
        public editDialogRef: MatDialogRef<F1OverviewComponent>,
        @Inject(MAT_DIALOG_DATA) public data: IEditDialogData,
        private _store: Store
      ) {
        this.pilots$ = this._store.select(getAllPilotsSelector);
        this.teams$ = this._store.select(getAllTeamsSelector);
        if(this.data.dialogType === DIALOG_TYPES.PILOT_DIALOG) {
            this._setupPilotsFg();
        } else {
            this._setupTeamsFg();
        }
    }

    editEntity(): void {
        if (this.data.dialogType === this.dialogTypes.PILOT_DIALOG) {
            this._store.dispatch(pilotsActions.editPilot({
                pilotId: this.data.id,
                pilot: {
                    name: this.dialogFg.controls['name'].value,
                    nationality: this.dialogFg.controls['nationality'].value,
                    teamId: this.dialogFg.controls['team'].value
                }
            }));
        } else {
            this._store.dispatch(teamsActions.editTeam({
                teamId: this.data.id,
                team: {
                    name: this.dialogFg.controls['name'].value,
                    engine: this.dialogFg.controls['engine'].value,
                    pilotIds: this.dialogFg.controls['pilotIds'].value
                }
            }));
        }
        this.editDialogRef.close();
    }

    closeDialog(): void {
        this.editDialogRef.close();
    }

    isFormValid(): boolean {
        return this.dialogFg.status === 'VALID';
    }

    private _setupTeamsFg() {
        this.teams$.pipe(
            first(),
            map( teams => {
                this.dialogFg = new FormGroup({
                    name: new FormControl(teams.find( team => team.id === this.data.id)?.name, [Validators.required]),
                    engine: new FormControl(teams.find( team => team.id === this.data.id)?.engine, [Validators.required]),
                    pilotIds: new FormControl(teams.find( team => team.id === this.data.id)?.pilotIds, [Validators.required])
                });
            })
        ).subscribe();
    }

    private _setupPilotsFg() {
        this.pilots$.pipe(
            first(),
            map( pilots => {
                this.dialogFg = new FormGroup({
                    name: new FormControl(pilots.find( pilot => pilot.id === this.data.id)?.name, [Validators.required]),
                    nationality: new FormControl(pilots.find( pilot => pilot.id === this.data.id)?.nationality, [Validators.required]),
                    team: new FormControl(pilots.find( pilot => pilot.id === this.data.id)?.teamId, [Validators.required])
                }); 
            })
        ).subscribe();
    }
}