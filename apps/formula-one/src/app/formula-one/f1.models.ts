export enum DIALOG_TYPES {
    PILOT_DIALOG = 'pilot',
    TEAM_DIALOG = 'team'
}

export interface IAddDialogData {
    dialogType: DIALOG_TYPES;
}

export interface IEditDialogData {
    dialogType: DIALOG_TYPES,
    id: string;
}