import { ComponentFixture, TestBed } from '@angular/core/testing';

import { F1OverviewComponent } from './f1-overview.component';

describe('F1OverviewComponent', () => {
  let component: F1OverviewComponent;
  let fixture: ComponentFixture<F1OverviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ F1OverviewComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(F1OverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
