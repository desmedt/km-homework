import { Component, OnDestroy, OnInit } from '@angular/core';
import { IF1Pilot, IF1Team } from '@km-homework/api-interfaces';
import { Store } from '@ngrx/store';
import { Observable, of, Subscription } from 'rxjs';
import { pilotsActions, teamsActions } from '../../root-store/f1-store/actions';
import { getAllPilotsSelector, getAllTeamsSelector } from '../../root-store/f1-store/f1-store.selectors';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { F1EditDialogComponent } from '../f1-edit-dialog/f1-edit-dialog.component';
import { DIALOG_TYPES } from '../f1.models';
import { F1AddDialogComponent } from '../f1-add-dialog/f1-add-dialog.comnponent';

@Component({
  selector: 'f1-overview',
  templateUrl: './f1-overview.component.html',
  styleUrls: ['./f1-overview.component.less']
})
export class F1OverviewComponent implements OnInit,OnDestroy {

  dialogTypes = DIALOG_TYPES;

  pilots$: Observable<IF1Pilot[]> = of([]);
  pilots: IF1Pilot[] = [];
  pilotsSub: Subscription;
  
  teams$: Observable<IF1Team[]> = of([]);
  teams: IF1Team[] = [];
  teamsSub: Subscription ;
  
  pilotColumns: string[] = ['name', 'nationality', 'teamName', 'buttons']
  teamsColumns: string[] = ['name', 'engine', 'pilotName', 'buttons']
  
  pilotsDataSource = new MatTableDataSource(this.pilots);
  teamsDataSource = new MatTableDataSource(this.teams);

  constructor(private _store: Store, private _dialogSvc: MatDialog) {
    this.pilots$ = this._store.select(getAllPilotsSelector);
    this.teams$ = this._store.select(getAllTeamsSelector);
    this.pilotsSub = this.pilots$.subscribe( pilots => {
      this.pilots = pilots;
      this.pilotsDataSource = new MatTableDataSource(pilots);
    });
    this.teamsSub = this.teams$.subscribe( teams => {
      this.teams = teams;
      this.teamsDataSource = new MatTableDataSource(teams);
    });
  }

  deleteTeam(teamId: string) {
    this._store.dispatch(teamsActions.deleteTeam({teamId: teamId}));
  }

  deletePilot(pilotId: string) {
    this._store.dispatch(pilotsActions.deletePilot({pilotId: pilotId}));
  }

  getTeamNames(teamId: string) {
    return this.teams.find( team => team.id === teamId)?.name
  }

  getPilotNames(pilotIds: string[]) {
    if (pilotIds) {
      return this.pilots.filter( pilot => pilotIds.includes(pilot.id)).map( pilot => pilot.name);
    } else {
      return []
    }
  }

  openEditDialog(dialogType: DIALOG_TYPES,id: string) {
    this._dialogSvc.open(F1EditDialogComponent, {
      data: {
        dialogType: dialogType,
        id: id
      }
    });
  }

  openAddDialog(dialogType: DIALOG_TYPES) {
    this._dialogSvc.open(F1AddDialogComponent, {
      data: {
        dialogType: dialogType
      }
    });
  }

  applyFilterPilots(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.pilotsDataSource.filter = filterValue.trim().toLowerCase();
  }

  applyFilterTeams(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.teamsDataSource.filter = filterValue.trim().toLowerCase();
  }

  ngOnInit(): void {
    this._store.dispatch(pilotsActions.getAllPilots());
    this._store.dispatch(teamsActions.getAllTeams());
  }

  ngOnDestroy(): void {
      this.teamsSub.unsubscribe();
      this.pilotsSub.unsubscribe();
  }
}
