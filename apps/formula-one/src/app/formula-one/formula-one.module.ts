import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { F1OverviewComponent } from './f1-overview/f1-overview.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { F1StoreModule } from '../root-store/f1-store/f1-store.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import { F1EditDialogComponent } from './f1-edit-dialog/f1-edit-dialog.component';
import { F1AddDialogComponent } from './f1-add-dialog/f1-add-dialog.comnponent';
import { MatSelectModule } from '@angular/material/select';


@NgModule({
  declarations: [
    F1OverviewComponent,
    F1EditDialogComponent,
    F1AddDialogComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    F1StoreModule,
    MatDialogModule,
    MatTableModule,
    MatButtonModule,
    MatInputModule,
    MatFormFieldModule,
    MatSelectModule,
    BrowserAnimationsModule,
  ]
})
export class FormulaOneModule { }
