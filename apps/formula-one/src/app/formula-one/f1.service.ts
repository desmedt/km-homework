import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IF1Pilot, IF1PilotRequest, IF1Team, IF1TeamRequest } from '@km-homework/api-interfaces';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class F1Service {

  constructor(private _httpSvc: HttpClient) {}

  getAllTeams(): Observable<IF1Team[]>  {
    return this._httpSvc.get<IF1Team[]>(`/api/teams`)
  }

  getAllPilots(): Observable<IF1Pilot[]> {
    return this._httpSvc.get<IF1Pilot[]>(`/api/pilots`)
  }

  getTeam(teamId: string): Observable<IF1Team> {
    return this._httpSvc.get<IF1Team>(`/api/teams/${teamId}`)
  }

  getPilot(pilotId: string): Observable<IF1Pilot> {
    return this._httpSvc.get<IF1Pilot>(`/api/teams/${pilotId}`)
  }

  deletePilot(pilotId: string): Observable<JSON> {
    return this._httpSvc.delete<JSON>(`/api/pilots/${pilotId}`);
  }

  deleteTeam(teamId: string): Observable<JSON> {
    return this._httpSvc.delete<JSON>(`/api/teams/${teamId}`);
  }

  addPilot(newPilot: IF1PilotRequest): Observable<IF1Pilot> {
    return this._httpSvc.post<IF1Pilot>('/api/pilots', newPilot);
  }

  addTeam(newTeam: IF1TeamRequest): Observable<IF1Team> {
    return this._httpSvc.post<IF1Team>('/api/teams', newTeam);
  }

  editTeam(teamId: string, team: IF1TeamRequest): Observable<IF1Team> {
    return this._httpSvc.put<IF1Team>(`/api/teams/${teamId}`, team);
  }

  editPilot(pilotId: string, pilot: IF1PilotRequest): Observable<IF1Pilot> {
    return this._httpSvc.put<IF1Pilot>(`/api/pilots/${pilotId}`, pilot);
  }
}
