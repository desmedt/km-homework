import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { FormulaOneModule } from './formula-one/formula-one.module';

@Module({
  imports: [FormulaOneModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
