import { ENGINE_TYPE, IF1PilotRequest, IF1TeamRequest } from '@km-homework/api-interfaces';
import { IsNotEmpty,IsString, IsArray} from 'class-validator';


export class AddTeamDto implements IF1TeamRequest {

    @IsNotEmpty()
    @IsString()
    name: string;

    @IsNotEmpty()
    @IsArray()
    pilotIds: string[];

    @IsNotEmpty()
    engine: ENGINE_TYPE;
}


export class AddPilotDto implements IF1PilotRequest {

    @IsNotEmpty()
    @IsString()
    name: string;

    @IsNotEmpty()
    @IsString()
    nationality: string;

    @IsNotEmpty()
    @IsString()
    teamId: string

}

export class EditPilotDto implements IF1PilotRequest {
    @IsNotEmpty()
    @IsString()
    name: string;

    @IsNotEmpty()
    @IsString()
    nationality: string;

    @IsNotEmpty()
    @IsString()
    teamId: string
}

export class EditTeamDto implements IF1TeamRequest {

    @IsNotEmpty()
    @IsString()
    name: string;

    @IsNotEmpty()
    @IsArray()
    pilotIds: string[];

    @IsNotEmpty()
    engine: ENGINE_TYPE;
}