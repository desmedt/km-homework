import { ENGINE_TYPE, IF1Pilot, IF1PilotRequest, IF1Team, IF1TeamRequest } from '@km-homework/api-interfaces';
import { Injectable } from '@nestjs/common';
import { v4 as uuid } from 'uuid';

@Injectable()
export class FormulaOneService {

    private _teams: IF1Team[];
    private _pilots: IF1Pilot[];

    constructor() {
        this._initData();
    }

    private _initData() {
        this._teams = [
            {
                id: uuid(),
                name: 'McLaren',
                engine: ENGINE_TYPE.ENGINE_1,
                pilotIds: ['pilotoneid','pilottwoid']
            },
            {
                id: uuid(),
                name: 'Red Bull Racing',
                engine: ENGINE_TYPE.ENGINE_2,
                pilotIds: ['pilotthreeid']
            },
            {
                id: uuid(),
                name: 'Renault F1 Team',
                engine: ENGINE_TYPE.ENGINE_3,
                pilotIds: ['pilotfourid']
            }
        ];
        this._pilots = [
            {
                name: 'Sebastian Vettel',
                nationality: 'German',
                teamId: this._teams[0].id,
                id: 'pilotoneid'
            },
            {
                name: 'Sergio Perez',
                nationality: 'Mexican',
                teamId: this._teams[0].id,
                id: 'pilottwoid'
            },
            {
                name: 'Max Verstappen',
                nationality: 'Dutch',
                teamId: this._teams[1].id,
                id: 'pilotthreeid'
            },
            {
                name: 'Fernando Alonso',
                nationality: 'Spanish',
                teamId: this._teams[2].id,
                id: 'pilotfourid'
            }
        ]
    }

    getAllPilots(): IF1Pilot[] {
        return this._pilots;
    }

    getPilot(id: string): IF1Pilot {
        return this._pilots.find( pilot => pilot.id == id);
    }

    getAllTeams(): IF1Team[] {
        return this._teams;
    }

    getTeam(id: string): IF1Team {
        return this._teams.find( team => team.id == id);
    }

    addNewPilot(pilot: IF1PilotRequest): IF1Pilot {
        const newPilot: IF1Pilot = {...pilot, id: uuid()}
        this._pilots.push(newPilot);
        this._addPilotToTeam(newPilot.id,newPilot.teamId);
        return newPilot;
    }

    addNewTeam(team: IF1TeamRequest): IF1Team {
        const newTeam: IF1Team = {...team, id: uuid()}
        team.pilotIds.forEach( pilotId => {
            this._updateAPilotsTeam(pilotId,newTeam.id);
            this._removePilotFromTeams(pilotId);   
        });
        this._teams.push(newTeam);
        return newTeam;
    }

    editTeam(teamId: string, teamData: IF1TeamRequest): IF1Team {
        const index = this._teams.findIndex( team => team.id === teamId);
        teamData.pilotIds.forEach( id => {
            this._removePilotFromTeams(id);
            this._updateAPilotsTeam(id,teamId);
        });
        this._teams[index] = {...this._teams[index], ...teamData};
        return this._teams[index];
    }

    editPilot(pilotId, pilotData: IF1PilotRequest): IF1Pilot {
        const index = this._pilots.findIndex( pilot => pilot.id === pilotId);
        this._pilots[index] = {...this._pilots[index],...pilotData};
        this._removePilotFromTeams(pilotId);
        this._addPilotToTeam(pilotId,pilotData.teamId);
        return this._pilots[index];
    }

    deletePilot(pilotId: string) {
        this._pilots = this._pilots.filter(pilot => pilot.id !== pilotId);
        this._removePilotFromTeams(pilotId);
    }

    deleteTeam(teamId: string) {
        this._teams = this._teams.filter(team => team.id !== teamId);
        this._removeTeamFromPilots(teamId);
    }

    private _removePilotFromTeams(pilotId: string) {
        this._teams.forEach(team => {
            if (team.pilotIds.includes(pilotId)) {
                team.pilotIds = team.pilotIds.filter(id => id !== pilotId)
            }
        });
    }

    private _removeTeamFromPilots(teamId: string) {
        this._pilots.forEach( pilot => {
            if (pilot.teamId === teamId) {
                pilot.teamId = '';
            }
        });
    }
    
    private _updateAPilotsTeam(pilotId: string, teamId: string) {
        this._pilots.forEach( pilot => {
            if (pilot.id === pilotId) {
                pilot.teamId = teamId;
            }
        });
    }

    private _addPilotToTeam(pilotId: string, teamId: string) {
        this._teams.forEach( team => {
            if (team.id === teamId) {
                team.pilotIds.push(pilotId);
            }
        });
    }
}
