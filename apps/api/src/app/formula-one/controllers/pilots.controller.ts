import { IF1Pilot } from '@km-homework/api-interfaces';
import { Body, Controller, Delete, Get, Param, Post, Put, Res } from '@nestjs/common';
import { FormulaOneService } from '../formula-one.service';
import { Response } from 'express';
import { AddPilotDto, EditPilotDto } from '../validators';

@Controller('pilots')
export class PilotsController {

    constructor(private _formulaOneSvc: FormulaOneService) {}

    @Get('/')
    getAllPilots(): IF1Pilot[] {
        return this._formulaOneSvc.getAllPilots();
    }

    @Get('/:id')
    getPilot(@Param() params): IF1Pilot {
        return this._formulaOneSvc.getPilot(params.id);
    }

    @Post('/')
    addNewPilot(@Body() addPilotDto: AddPilotDto, @Res() res: Response) {
        res.send(this._formulaOneSvc.addNewPilot(addPilotDto)).json;
    }

    @Put('/:id')
    editPilot(@Param() params, @Body() editPilotDto: EditPilotDto, @Res() res: Response) {
        res.send(this._formulaOneSvc.editPilot(params.id,editPilotDto)).json;
    }

    @Delete('/:id')
    deletePilot(@Param() params, @Res() res: Response) {
        this._formulaOneSvc.deletePilot(params.id);
        res.send({result: 'ok'}).json;
    }
}