import { IF1Team } from '@km-homework/api-interfaces';
import { Body, Controller, Delete, Get, Param, Post, Put, Res } from '@nestjs/common';
import { FormulaOneService } from '../formula-one.service';
import { Response} from 'express';
import { AddTeamDto, EditTeamDto } from '../validators';

@Controller('teams')
export class TeamsController {

    constructor(private _formulaOneSvc: FormulaOneService) {}

    @Get('/')
    getAllTeams(): IF1Team[] {
        return this._formulaOneSvc.getAllTeams();
    }

    @Get('/:id')
    getTeam(@Param() params): IF1Team {
        return this._formulaOneSvc.getTeam(params.id);
    }

    @Post('/')
    addNewTeam(@Body() addTeamDto: AddTeamDto, @Res() res: Response) {
        res.send(this._formulaOneSvc.addNewTeam(addTeamDto)).json;
    }

    @Put('/:id')
    editTeam(@Param() params, @Body() editTeamDto: EditTeamDto, @Res() res: Response) {
        res.send(this._formulaOneSvc.editTeam(params.id,editTeamDto)).json;
    }

    @Delete('/:id')
    deleteTeam(@Param() params, @Res() res: Response) {
        this._formulaOneSvc.deleteTeam(params.id);
        res.send({result: 'ok'}).json;
    }
}
