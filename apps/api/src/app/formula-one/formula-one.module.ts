import { Module } from '@nestjs/common';
import { FormulaOneService } from './formula-one.service';
import { PilotsController } from './controllers/pilots.controller';
import { TeamsController } from './controllers/teams.controller';

@Module({
  controllers: [ PilotsController, TeamsController],
  providers: [FormulaOneService]
})
export class FormulaOneModule {}
