
// Overview
export const newTeamButton = () => cy.get('[id=addTeamButton]');
export const newPilotButton = () => cy.get('[id=addPilotButton]');
export const overviewMainContent = () => cy.get('.main');
export const overviewTables = () => cy.get('table');
export const tableRows = () => cy.get('table tr');
export const deletePilotButtons = () => cy.get('[id=deletePilotButton]');
export const deleteTeamButtons = () => cy.get('[id=deleteTeamButton]')
export const editPilotButtons = () => cy.get('[id=editPilotButton]');
export const editTeamButtons = () => cy.get('[id=editTeamButton]');

// Add Dialog
export const addDialogMainContent = () => cy.get('add-dialog-main');
export const closeAddDialogButton = () => cy.get('[id=closeAddDialogButton]');
export const addNewEntityButton = () => cy.get('[id=addNewEntityButton]');
export const addDialogInputs = () => cy.get('input');
export const addDialogSelectPilot = () => cy.get(' mat-select[formControlName="pilotIds"]');
export const addDialogSelectEngine = () => cy.get('mat-select[formControlName="engine"]');

// Edit Dialog
export const editDialogMainContent = () => cy.get('edit-dialog-main');
export const closeEditDialogButton = () => cy.get('[id=closeEditDialogButton]');
export const editEntityButton = () => cy.get('[id=editEntityButton]');