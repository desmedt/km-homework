import { addDialogInputs, addDialogSelectEngine, addDialogSelectPilot, addNewEntityButton, closeAddDialogButton, deletePilotButtons, newPilotButton, newTeamButton, overviewMainContent, overviewTables, tableRows} from "../support/f1.po";


describe('F1 Overview Component', () => {
    
    it('F1 Overview Component Should Be Visible', () => {
        cy.visit('/');
        overviewMainContent().should('be.visible');
        tableRows().should('have.length',9);
    });

    it('Overview Elements Should Be Visible', () => {
        overviewTables().should('be.visible');
        overviewTables().should('have.length',2);
        newTeamButton().should('be.visible');
        newPilotButton().should('be.visible');
    });

    it('Should Open and Close Add New Team Dialog', () => {
        newTeamButton().click();
        closeAddDialogButton().should('be.visible');
        closeAddDialogButton().click();
        closeAddDialogButton().should('not.exist');
    });

    it('Should Delete A Pilot', () => {
        deletePilotButtons().should('have.length',4);
        deletePilotButtons().eq(0).click();
        tableRows().should('have.length',8);
    })

    /* it('Should Add A New Team', () => {
        newTeamButton().click();
        closeAddDialogButton().should('be.visible');
        addDialogInputs().should('have.length',3);
        addDialogInputs().eq(0).type('test',{force: true});
        addDialogSelectEngine().should('be.visible');
        addDialogSelectPilot().should('be.visible');
        addDialogSelectEngine().click();
        cy.get('mat-option').contains('unknown').click();
        addDialogSelectPilot().click();
        cy.get('mat-option').contains('pilot-1').click();
        addDialogSelectPilot().type('{esc}',{force: true});
        addNewEntityButton().click();
        closeAddDialogButton().should('not.exist');
        cy.get('table tr').should('have.length',10);
    }) */
});