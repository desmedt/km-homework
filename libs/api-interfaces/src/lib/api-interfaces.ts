export interface IF1Pilot extends IF1PilotRequest {
    id: string;
}

export interface IF1Team extends IF1TeamRequest {
    id: string;
}

export enum ENGINE_TYPE {
    UNKNOWN = 'unknown',
    ENGINE_1 = 'engine type 1',
    ENGINE_2 = 'engine type 2',
    ENGINE_3 = 'engine type 3'
}

export interface IEngine {
    type: ENGINE_TYPE
}

export const allEngines: IEngine[] = [
    {
        type: ENGINE_TYPE.ENGINE_1
    },
    {
        type: ENGINE_TYPE.ENGINE_2
    },
    {
        type: ENGINE_TYPE.ENGINE_3
    },
    {
        type: ENGINE_TYPE.UNKNOWN
    }
]

export interface IF1PilotRequest {
    name: string;
    teamId: string,
    nationality: string;
}

export interface IF1TeamRequest {
    name: string;
    engine: ENGINE_TYPE,
    pilotIds: string[]
}