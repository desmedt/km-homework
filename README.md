## pre-req:
1. node lts
2. docker

## To run dev env:

1. npm install
2. start api with `npx nx serve api`
3. start client with `npx nx serve`
4. open `localhost:4200`

## Docker build + run
in repo root:
1. build fe image `docker build -t frontend -f apps/formula-one/Dockerfile.fe .`
2. run fe container `docker run -p 80:80 -d frontend`
3. build be image `docker build -t backend -f apps/api/Dockerfile.api .`
4. run be container `docker run -d -p 3333:3333 backend`
be test response `curl localhost:3333/api/teams`
fe test response `curl localhost:80`

## Run tests:

1. start api in dev mode: `npx nx serve api`
2. start e2e tests: `npx nx e2e formula-one-e2e --baseUrl=http://localhost:4200`

Reseting the api after test runs is neccessary as tests are asserted against the original state and
will fail after multiple runs.